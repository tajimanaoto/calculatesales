package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

//	商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

//	商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";

//	追加メッセージ
	private static final String SALE_NUMBER_ERROR = "売り上げファイル名が連番になっていません。";
	private static final String SALE_AMOUNT_ERROR = "売上金額が10桁を超えています。";
	private static final String BRANCH_CODE_ERROR = "の支店コードが不正です";
	private static final String COMMODITY_CODE_ERROR = "の商品コードが不正です";
	private static final String FORMAT_ERROR = "のフォーマットが不正です";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

//		商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

//		商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();


//		コマンドライン引数が渡されているかを確認
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}


		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", "^[0-9]{3}")) {
			return;
		}

//		商品定義ファイルの読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品", "^[A-Za-z0-9]{8}")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
//		args[0](コマンドライン引数)に指定したpassの中にあるfileを取得してfilesに代入する
		File[] files = new File(args[0]).listFiles();

//		＜＞の中が同じであれば省略できるので右辺に記述しなくても大丈夫
//		右辺がArrayListと記述なのはnewがListというものに使用できないから
		List<File> rcdFiles = new ArrayList<>();

//		ファイル名が「数字8桁.rcd」なのかを確認
//		ファイルなのかそれ以外なのか、そのファイルが0～9の数字8桁で始まりrcdで終わっているか確認。
//		当てはまっていたらtrue、それ以外ならfalse
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")){
				rcdFiles.add(files[i]);
			}
		}


//		rcdファイルを連番にする
		Collections.sort(rcdFiles);


		//rcdファイルが連番になっているか確認
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(SALE_NUMBER_ERROR);
				return;
			}
		}

//		rcdファイルのデータをbranchSalesにput
		for (int i = 0; i < rcdFiles.size(); i++) {
			File rcdFile = rcdFiles.get(i);
			BufferedReader br = null;

			try {
				FileReader fr = new FileReader(rcdFile);
				br = new BufferedReader(fr);

//				rcdファイルを1行ずつ読み込み、リストに入れる
				String line;
				List<String> prise = new ArrayList<>();

//				1行ずつ読み取ったものがnullになるまで繰り返し処理を行う
				while((line = br.readLine()) != null) {
					prise.add(line);
				}

//				rcdファイルのフォーマットが正しいかを確認
				if (prise.size() != 3) {
					System.out.println(rcdFile.getName() + FORMAT_ERROR);
					return;
				}

//				支店コードが存在するかを確認
				if (!branchNames.containsKey(prise.get(0))) {
					System.out.println(rcdFile.getName() + BRANCH_CODE_ERROR);
					return;
				}
//				商品コードが存在するかを確認
				if (!commodityNames.containsKey(prise.get(1))) {
					System.out.println(rcdFile.getName() + COMMODITY_CODE_ERROR);
					return;
				}
				
//				売上金額が数字か確認
				if (!prise.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

//				売り上げ金額が文字列なのを、整数型に変換する処理(rcdファイルの中の3行目)
				Long fileSales = Long.parseLong(prise.get(2));

//				Mapから取得してきた、売り上げ金額を支店別に加算し合計を出す処理	
//				branchSales.get(prise.get(0))で、keyを呼び出しているので紐づいているvalueの事を指している
				Long branchSaleAmount = branchSales.get(prise.get(0)) + fileSales;
				
//				Mapから取得してきた、売り上げ金額を商品別に加算し合計を出す処理	
				Long commoditySaleAmount = commoditySales.get(prise.get(1)) + fileSales;

//				売上金額の合計が10桁を超えていないか確認
				if(branchSaleAmount >= 10000000000L || commoditySaleAmount >= 1000000000L){
					System.out.println(SALE_AMOUNT_ERROR);
					return;
				}
				
//				branchSalesと指定したMapに、追加する処理
				branchSales.put(prise.get(0), branchSaleAmount);
				
//				commoditySalesと指定したMapに、追加する処理
				commoditySales.put(prise.get(1), commoditySaleAmount);

			}catch(IOException e){
				System.out.println(UNKNOWN_ERROR);
				return;

			}finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

//		 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> namesMap,
			Map<String, Long> salesMap, String branchesOrItems, String regex ) {

//		BufferedReaderの初期化,
//		try節の外で初期化しておかないと、finally節で参照できない
		BufferedReader br = null;

		try {

//			FileReaderを使用するために作成している
			File file = new File(path, fileName);

//			ファイルやディレクトリが存在しているか確認
//			今回は商品定義ファイル・支店定義ファイルが存在しているか確認
			if(!file.exists()) {
				System.out.println(branchesOrItems + FILE_NOT_EXIST);
				return false;
			}

//			BufferedReaderを使用するために作成している
			FileReader fr = new FileReader(file);


//			File・FileReaderがあるから使用できる
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");


				//支店定義ファイル・商品定義ファイルのフォーマットの確認
				if ((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(branchesOrItems + FILE_INVALID_FORMAT);
					return false;
				}

				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap, Map<String, Long> salesMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		File branchOut = new File(path, fileName);

//		BufferedWriterの初期化,
//		try節の外で初期化しておかないと、finally節で参照できない
		BufferedWriter bw = null;

		try {

//			ファイルの書き込み
			FileWriter fw = new FileWriter(branchOut);
			bw = new BufferedWriter(fw);

			// 支店コード,支店名,合計を書き込んでいる
			for (String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
